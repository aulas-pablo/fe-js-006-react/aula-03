// @ts-nocheck

import { useState, useEffect, useRef, useMemo } from 'react';



interface ITodo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

export function App() {
  // const [todos, setTodos] = useState<ITodo[]>([]);
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState("")
  const [todos, setTodos] = useState([])

  const handleFetchTodos = async () => {
    await fetchTodos()
  }

  const handleInputText = (event) => {
    setSearchText(event.target.value)
  }


  const todosFiltereded = useMemo(() => {
    const searchTextLowerCase = searchText?.toLowerCase()
    const todosFilter = todos?.filter(todo => todo.title?.toLowerCase().includes(searchTextLowerCase))
    return todosFilter
  }, [todos, searchText])

  const fetchTodos = async () => {
    try {
      setLoading(true);
      const response = await fetch(
        'https://jsonplaceholder.typicode.com/todos'
      );
      const data: ITodo[] = await response.json();
      setTodos(data)
    } catch (err) {
      console.log(err);
    } finally {
      setTimeout(() => {
        setLoading(false);
      }, 1000)
    }
  };

  useEffect(() => {
    fetchTodos();
  }, []);



  return (
    <>
      <div style={{ display: 'flex', flexDirection: 'column', padding: 10 }}>
        <div style={{ display: 'flex', gap: 10 }}>
          <input style={{ borderRadius: 5, padding: 10, minWidth: 400 }} type="text"
            placeholder='Pesquisar'
            onChange={handleInputText} />
          <button style={{ color: 'green' }} onClick={handleFetchTodos}>Buscar</button>
        </div>

        {loading ? (
          <span>Carregando...</span>
        ) : (
          <ul>
            {todosFiltereded?.map((todo, key) => (
              <li key={key}>{todo.title}</li>
            ))}
          </ul>
        )}
      </div>
    </>
  );
}

export default App
