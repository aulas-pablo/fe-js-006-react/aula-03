import { useState, useEffect } from 'react';

export function ExemploCorrida() {
  const [contadorA, setContadorA] = useState(0);
  const [contadorB, setContadorB] = useState(0);

  useEffect(() => {
    // Atualizando ambos os estados em um intervalo de tempo
    const intervalId = setInterval(() => {
      setContadorA(contadorA + 1);
      setContadorB(contadorB + 1);
    }, 1000);

    return () => clearInterval(intervalId); // Limpar intervalo ao desmontar o componente
  }, [contadorA, contadorB]); // Efeito é dependente dos contadores A e B

  return (
    <div>
      <p>Contador A: {contadorA}</p>
      <p>Contador B: {contadorB}</p>
    </div>
  );
}